package com.tuv001.medizinplatform;
/**
 * Created by JTJ
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PatientMain extends AppCompatActivity {

    private Button symtomsquery;
    private Button medicamentsranking;
    private Button medicaldiagnostic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_main);



        //Button symtomsquery
        symtomsquery =(Button)findViewById(R.id.buttonanfrage);
        symtomsquery.setOnClickListener(new View.OnClickListener() {//if click on the button Anfrage

            @Override
            public void onClick(View v) {
                Toast.makeText(PatientMain.this,getResources().getString(R.string.messagetellme),Toast.LENGTH_LONG).show();
                Intent intent=new Intent(PatientMain.this,Symtoms_Anfrage.class);//If click on the button, then redirect to Symtoms_Anfrage activity
                startActivity(intent); //Start
            }
        });

        //Button medicamentsranking

        medicamentsranking=(Button)findViewById(R.id.buttonmedikament);
        medicamentsranking.setOnClickListener(new View.OnClickListener() {//if click on the button Medikament
            @Override
            public void onClick(View v) {
                Toast.makeText(PatientMain.this,getResources().getString(R.string.informationbestmedicine),Toast.LENGTH_LONG).show();
                Intent intent=new Intent(PatientMain.this,Medikament.class);//If click on the button, then redirect to Medikament activity

                startActivity(intent);
            }
        });

        //Button Medical
        medicaldiagnostic=(Button)findViewById(R.id.buttonDiagnostik);
        medicaldiagnostic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PatientMain.this,getResources().getString(R.string.messageyourmedical),Toast.LENGTH_LONG).show();
                Intent intent=new Intent(PatientMain.this,MedizinischeDiagnostik.class);//If click on the button, then redirect to MedizinischeDiagnostik activity
                startActivity(intent);
        }
        });
    }
}
