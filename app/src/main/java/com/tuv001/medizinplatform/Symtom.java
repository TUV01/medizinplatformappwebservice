package com.tuv001.medizinplatform;

/**
 * Created by JTJ-PC(Tuv01) on 01.05.2017.
 */

public class Symtom
{
    String name;
    boolean checkbox;

    public Symtom() {
         /*Empty Constructor*/
    }
    public Symtom(String name, boolean status){
        this.name = name;
        this.checkbox = status;
    }
    //Getter and Setter
    public String getSymtom() {
        return name;
    }

    public void setSymtom(String name) {
        this.name = name;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }
}
